# Docker image: joeysbytes/python

## Description

This is a base image for Python projects.

| Feature               | Value                                                                                                                                                       |
|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| User                  | pyuser                                                                                                                                                      |
| Group                 | pygroup                                                                                                                                                     |
| User SSH Directory    | /home/pyuser/.ssh                                                                                                                                           |
| User bin Directory    | /home/pyuser/.local/bin (on PATH)<br />/home/pyuser/bin (symlink)                                                                                           |
| Project Directories   | /project<br />/project/bin (on PATH)<br />/project/config<br />/project/data<br />/project/secrets (only pyuser access)<br />/project/src<br />/project/tmp |
| Environment Variables | PROJECT_DIR                                                                                                                                                 |
| Additional Software   | nano                                                                                                                                                        |
