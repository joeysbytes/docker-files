# This image is built on top of python-base image
FROM joeysbytes/python-base

# Install apt software
USER root
RUN source "/docker_scripts/bash/common.bash" && \
    source "/docker_scripts/bash/apt.bash" && \
    apt_install_packages "iputils-ping" && \
    apt_clear_cache && \
    which ssh-keygen && \
    which ssh-keyscan && \
    which sort && \
    which uniq && \
    which ping

# Install Ansible
USER pyuser
RUN source "/docker_scripts/bash/common.bash" && \
    source "/docker_scripts/bash/python.bash" && \
    pip_install_packages "ansible" && \
    ansible --version && \
    pip_clear_cache

# Install ansible-action
RUN source "/docker_scripts/bash/common.bash" && \
    mkdir "/project/src/ansible-action" && \
    set_permissions "/project/src/ansible-action" "755" "" "" && \
    cd "/project/src/ansible-action" && \
    curl --no-progress-meter \
      --output "./ansible-action.tar.gz" \
      "https://gitlab.com/joeysbytes/ansible-action/-/archive/main/ansible-action-main.tar.gz" && \
    tar --strip-components=1 -xzf "./ansible-action.tar.gz" && \
    chmod 755 ./bash_scripts/* && \
    ln -s "/project/src/ansible-action/bash_scripts/ansible-action.bash" "/project/bin/ansible-action" && \
    rm "./ansible-action.tar.gz"

# Volumes
VOLUME /home/pyuser/.ssh

# Set Metadata
LABEL net.joeysbytes.description="Ansible"
LABEL net.joeysbytes.source_code="https://gitlab.com/joeysbytes/docker-files/ansible"
LABEL net.joeysbytes.parent_image="joeysbytes/python-base"
