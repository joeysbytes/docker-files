# helper_scripts

Helper scripts to assist in building / running Docker images.

This Dockerfile snippet will install the bash scripts to /docker_scripts/bash:

```Dockerfile
SHELL ["/bin/bash", "-c"]
RUN mkdir "/docker_scripts" && \
    chmod 777 "/docker_scripts" && \
    cd "/docker_scripts" && \
    curl --no-progress-meter \
      --output "./helper_scripts.tar.gz" \
      "https://gitlab.com/joeysbytes/docker-files/-/archive/main/docker-files-main.tar.gz?path=helper_scripts" && \
    tar --strip-components=2 -xzf "./helper_scripts.tar.gz" && \
    chmod 666 ./bash/*.bash && \
    rm "./helper_scripts.tar.gz"
```

Then source the helper scripts you need. Note that you should always source common.bash. For example:

```Dockerfile
RUN source "/docker_scripts/bash/common.bash" && \
    source "/docker_scripts/bash/apt.bash" && \
    source "/docker_scripts/bash/apt_software.bash" && \
    source "/docker_scripts/bash/python.bash" && \
    # continue commands here ...
```

Finally, if you do not need the helper scripts anymore, you can delete the files.
