###########################################################################
# Update pip to latest version.
# (It's never on the latest version and throws a warning.)
###########################################################################
function update_pip() {
    must_be_root
    python -m pip install --upgrade pip
}


###########################################################################
# Clear pip cache
###########################################################################
function pip_clear_cache() {
    pip cache purge
    rm -rf "$(pip cache dir)"
}


###########################################################################
# Install packages via pip, at the user level.
#
# Parameters:
#   1 - a space-separated list of pip packages to install
###########################################################################
function pip_install_packages() {
    if [ $# -ne 1 ]
    then
        echo "USAGE: pip_install_packages packageList"
        exit 1
    fi
    local packages="${1}"
    # shellcheck disable=SC2086
    python -m pip install --user ${packages}
}
