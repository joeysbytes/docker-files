###########################################################################
# Verifies that you are currently the root user, exits if not
###########################################################################
function must_be_root() {
    local user_name
    user_name=$(whoami)
    if [ ! "${user_name}" == "root" ]
    then
        echo "ERROR: Not root user: ${user_name}"
        exit 1
    fi
}


###########################################################################
# Adds a group
#
# Parameters:
#   1 - Group Name
#   2 - Group ID
###########################################################################
function add_group() {
    must_be_root
    if [ $# -ne 2 ]
    then
        echo "USAGE: add_group groupName groupId"
        exit 1
    fi
    local group_name="${1}"
    local group_id="${2}"
    groupadd --non-unique --gid "${group_id}" "${group_name}"
}


###########################################################################
# Adds a user, and fully sets up the home and project directories.
#
# Parameters:
#   1 - User Name
#   2 - Group Name
#   3 - User ID
#   4 - Group ID
###########################################################################
function add_user() {
    must_be_root
    if [ $# -ne 4 ]
    then
        echo "USAGE: add_user userName groupName userId groupId"
        exit 1
    fi
    local user_name="${1}"
    local group_name="${2}"
    local user_id="${3}"
    local group_id="${4}"
    # Create group
    add_group "${group_name}" "${group_id}"
    # Set up user
    useradd --non-unique --create-home --no-user-group --shell /bin/bash \
      --uid "${user_id}" --gid "${group_id}" "${user_name}"
    # Set up user home directories
    mkdir "/home/${user_name}/.local"
    set_permissions "/home/${user_name}/.local" "755" "${user_id}" "${group_id}"
    mkdir "/home/${user_name}/.local/bin"
    set_permissions "/home/${user_name}/.local/bin" "755" "${user_id}" "${group_id}"
    ln -s "/home/${user_name}/.local/bin" "/home/${user_name}/bin"
    mkdir "/home/${user_name}/.ssh"
    set_permissions "/home/${user_name}/.ssh" "700" "${user_id}" "${group_id}"
    # Set up user configuration files
    curl --no-progress-meter --output "/home/${user_name}/.bash_aliases" \
      "https://gitlab.com/joeysbytes/config-files/-/raw/main/bash/.bash_aliases"
    set_permissions "/home/${user_name}/.bash_aliases" "644" "${user_id}" "${group_id}"
    # Install nano config
    if [ "$(which nano)" ]
    then
        local nano_version
        nano_version=$(nano --version|head -1|cut -f5 -d\ |cut -c1)
        curl --no-progress-meter --output "/home/${user_name}/.nanorc" \
          "https://gitlab.com/joeysbytes/config-files/-/raw/main/nano/.nanorc${nano_version}"
        set_permissions "/home/${user_name}/.nanorc" "644" "${user_id}" "${group_id}"
    fi
}


###########################################################################
# Creates project directories, and sets them to the provided ownership.
#
# Parameters:
#   1 - User Id (chown)
#   2 - Group Id (chown)
###########################################################################
function create_project_directories() {
    must_be_root
    if [ $# -ne 2 ]
    then
        echo "USAGE: create_project_directories userId groupId"
        exit 1
    fi
    local user_id="${1}"
    local group_id="${2}"
    mkdir "/project"
    set_permissions "/project" "755" "${user_id}" "${group_id}"
    mkdir "/project/bin"
    set_permissions "/project/bin" "755" "${user_id}" "${group_id}"
    mkdir "/project/config"
    set_permissions "/project/config" "755" "${user_id}" "${group_id}"
    mkdir "/project/data"
    set_permissions "/project/data" "755" "${user_id}" "${group_id}"
    mkdir "/project/secrets"
    set_permissions "/project/secrets" "700" "${user_id}" "${group_id}"
    mkdir "/project/src"
    set_permissions "/project/src" "755" "${user_id}" "${group_id}"
    mkdir "/project/tmp"
    set_permissions "/project/tmp" "755" "${user_id}" "${group_id}"

    # Create symlinks if home directory exists
    local user_name
    user_name=$(id -nu "${user_id}")
    if [ -d "/home/${user_name}" ]
    then
        ln -s "/project" "/home/${user_name}/project"
    fi
}


###########################################################################
# Given a file or directory, change the permissions based on the passed-in
# values.  All values must be supplied, but if any value is an empty
# string, it will be ignored (except for the path).
#
# Parameters:
#   1 - File or Directory
#   2 - File or Directory mode, or empty string (chmod)
#   3 - User ID / Name, or empty string (chown)
#   4 - Group ID / Name, or empty string (chgrp)
###########################################################################
function set_permissions() {
    if [ $# -ne 4 ]
    then
        echo "USAGE: set_permissions fileOrDirectoryPath mode userId groupId"
        exit 1
    fi
    local path_name="${1}"
    local path_mode="${2}"
    local user_id="${3}"
    local group_id="${4}"
    if [ ! "${user_id}" == "" ]
    then
        chown "${user_id}" "${path_name}"
    fi
    if [ ! "${group_id}" == "" ]
    then
        chgrp "${group_id}" "${path_name}"
    fi
    if [ ! "${path_mode}" == "" ]
    then
        chmod "${path_mode}" "${path_name}"
    fi
}


###########################################################################
# Delete all files from /tmp
###########################################################################
function clear_tmp_directory() {
    # shellcheck disable=SC2164
    cd /tmp
    rm -rf ./*
}
