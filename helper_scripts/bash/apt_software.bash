###########################################################################
# Installs nano, and sets up the nano config file if a UID is provided.
#
# Parameters:
#   1 - UID (optional) of user, or empty string to ignore
#   2 - GID (optional) of group, or empty string to ignore
###########################################################################
function apt_install_nano() {
    must_be_root
    if [ $# -ne 2 ]
    then
        echo "USAGE: apt_install_nano userId groupId"
        exit 1
    fi
    local user_id="${1}"
    local group_id="${2}"
    # Install nano
    apt_install_packages "nano"
    # Install nano config file
    local user_name
    user_name=$(id -nu "${user_id}")
    if [ -d "/home/${user_name}" ]
    then
        local nano_version
        nano_version=$(nano --version|head -1|cut -f5 -d\ |cut -c1)
        curl --no-progress-meter --output "/home/${user_name}/.nanorc" \
          "https://gitlab.com/joeysbytes/config-files/-/raw/main/nano/.nanorc${nano_version}"
        set_permissions "/home/${user_name}/.nanorc" "644" "${user_id}" "${group_id}"
    fi
}
