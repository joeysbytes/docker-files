# shellcheck disable=SC2034
DEBIAN_FRONTEND=noninteractive

###########################################################################
# Update the system via apt
###########################################################################
function apt_update_system() {
    must_be_root
    apt-get update
    apt-get --yes upgrade
}


###########################################################################
# Clear the apt cache
###########################################################################
function apt_clear_cache() {
    must_be_root
    apt-get clean
    rm -rf /var/lib/apt/lists/*
}


###########################################################################
# Install packages via apt.
#
# Parameters:
#   1 - a space-separated list of apt packages to install
###########################################################################
function apt_install_packages() {
    must_be_root
    if [ $# -ne 1 ]
    then
        echo "USAGE: apt_install_packages packageList"
        exit 1
    fi
    local packages="${1}"
    apt-get update
    # shellcheck disable=SC2086
    apt-get install --yes --no-install-recommends ${packages}
}
