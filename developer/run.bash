#!/usr/bin/env bash
# shellcheck disable=SC1090
set -e

###########################################################################
# Global Variables
###########################################################################
# Constants
BASE_DIR="$(realpath ..)"
DOCKER_DIR="${BASE_DIR}/docker_files"
DEVELOPER_DIR="${BASE_DIR}/developer"
FUNCTIONS_DIR="${DEVELOPER_DIR}/functions"
VARIABLES_DIR="${DEVELOPER_DIR}/variables"
ENVIRONMENT_FILES_DIR="${DEVELOPER_DIR}/env_files"

# These values will be set by the script
DOCKER_PROJECT=""
DOCKER_PROJECT_DIR=""
DOCKER_PROJECT_TYPE=""
ACTION=""
ENV_FILE=""

# See also _defaults.bash variables file

###########################################################################
# Main Processing Logic
###########################################################################
function main() {
    get_arguments "$@"
    get_docker_type
    get_variables
    get_environment_file
    show_run_info
    execute_action
}


###########################################################################
# Initialization / Validations
###########################################################################

#--------------------------------------------------------------------------
# Get the passed-in arguments
#--------------------------------------------------------------------------
function get_arguments() {
    # Validate usage
    if [ $# -ne 2 ]
    then
        show_usage
        exit 1
    fi
    # Get arguments
    DOCKER_PROJECT="${1}"
    DOCKER_PROJECT_DIR="${DOCKER_DIR}/${DOCKER_PROJECT}"
    ACTION="${2}"
}


#--------------------------------------------------------------------------
# Determine if plain Docker project or Docker Compose project
#--------------------------------------------------------------------------
function get_docker_type() {
    # Check that Docker project exists
    if [ ! -d "${DOCKER_PROJECT_DIR}" ]
    then
        echo "ERROR: Docker project directory not found: ${DOCKER_PROJECT_DIR}" 1>&2
        exit 1
    fi
    # Check if this is a standard Docker project or a Docker Compose project
    if [ -f "${DOCKER_PROJECT_DIR}/docker-compose.yaml" ] || [ -f "${DOCKER_PROJECT_DIR}/docker-compose.yml" ]
    then
        DOCKER_PROJECT_TYPE="compose"
    elif [ -f "${DOCKER_PROJECT_DIR}/Dockerfile" ]
    then
        DOCKER_PROJECT_TYPE="docker"
    else
        echo "ERROR: Did not any find Docker files: ${DOCKER_PROJECT_DIR}" 1>&2
        exit 1
    fi
}


#--------------------------------------------------------------------------
# Get Docker project-specific variables
#--------------------------------------------------------------------------
function get_variables() {
    source "${VARIABLES_DIR}/_defaults.bash"
    local var_file="${VARIABLES_DIR}/${DOCKER_PROJECT}.bash"
    if [ ! -f "${var_file}" ]
    then
        echo "ERROR: Variables file not found: ${var_file}" 1>&2
        exit 1
    fi
    source "${var_file}"
}


#--------------------------------------------------------------------------
# Search for Docker Compose environment file (optional)
#--------------------------------------------------------------------------
function get_environment_file() {
    local environment_file="${ENVIRONMENT_FILES_DIR}/${DOCKER_PROJECT}.env"
    if [ -f "${environment_file}" ]
    then
        ENV_FILE="${environment_file}"
    fi
}


#--------------------------------------------------------------------------
# Show Usage of this script
#--------------------------------------------------------------------------
function show_usage() {
    echo "USAGE: run.bash dockerProject action"
    echo "ACTIONS:"
    echo "  adhoc:      Run an adhoc function"
    echo "  build:      Build Docker image"
    echo "  containers: List containers"
    echo "  down:       Stop running container"
    echo "  images:     List images"
    echo "  prune:      Delete dangling images / volumes"
    echo "  shell:      Enter shell prompt within container"
    echo "  up:         Start container"
}


#--------------------------------------------------------------------------
# Show values we are running with
#--------------------------------------------------------------------------
function show_run_info() {
    echo "=========================="
    echo "Docker Developer Assistant"
    echo "=========================="
    echo "Project:    ${DOCKER_PROJECT}"
    echo "Type:       ${DOCKER_PROJECT_TYPE}"
    echo "Action:     ${ACTION}"
    echo "Image Name: ${IMAGE_NAME}"
    echo "Cont. Name: ${CONTAINER_NAME}"
    echo "Env File:   ${ENV_FILE}"
    echo ""
}


###########################################################################
# Execute the requested action
###########################################################################
function execute_action() {
    source "${FUNCTIONS_DIR}/common_functions.bash"
    source "${FUNCTIONS_DIR}/${DOCKER_PROJECT_TYPE}_functions.bash"
    # Many warnings / errors can occur during Docker operations, we do not
    # want the script to exit if this happens.
    set +e
    case "${ACTION}" in
        "adhoc")
            adhoc
            ;;
        "build")
            docker_build
            ;;
        "containers")
            docker_containers
            ;;
        "down")
            docker_down
            ;;
        "images")
            docker_images
            ;;
        "prune")
            docker_prune
            ;;
        "shell")
            docker_shell
            ;;
        "up")
            docker_up
            ;;
        *)
            echo "ERROR: Unknown action: ${ACTION}" 1>&2
            show_usage
            exit 1
            ;;
    esac
    set -e
}

###########################################################################
# Start the script
###########################################################################
main "$@"
