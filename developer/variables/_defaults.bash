# These are variables which should be defined in each project-specific variable file.

IMAGE_NAME=""
CONTAINER_NAME=""
CONTAINER_SHELL="/bin/bash"

# Flags for build action, empty string=ignore
BUILD_NO_CACHE=""
BUILD_PULL=""

# Flags for up action, empty string=ignore
UP_DETACH="yes"
