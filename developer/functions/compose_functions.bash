###########################################################################
# Build a Docker Image
###########################################################################
function docker_build() {
    local command
    command="$(_start_docker_compose_command)"
    command="${command} build --progress plain"
    if [ ! "${BUILD_NO_CACHE}" == "" ]
    then
        command="${command} --no-cache"
    fi
    if [ ! "${BUILD_PULL}" == "" ]
    then
        command="${command} --pull"
    fi
    eval "${command}"
    docker_prune
    docker_images
}


###########################################################################
# Start a Docker Container
###########################################################################
function docker_up() {
    local command
    command="$(_start_docker_compose_command)"
    command="${command} up"
    if [ ! "${UP_DETACH}" == "" ]
    then
        command="${command} --detach"
    fi
    eval "${command}"
    docker_containers
}


###########################################################################
# Stop a Docker Container
###########################################################################
function docker_down() {
    local command
    command="$(_start_docker_compose_command)"
    command="${command} down"
    eval "${command}"
    docker_containers
}

###########################################################################
# Helper Functions
###########################################################################

#--------------------------------------------------------------------------
# Build the initial docker compose command
#--------------------------------------------------------------------------
function _start_docker_compose_command() {
    local cmd="docker compose --project-directory ${DOCKER_PROJECT_DIR}"
    if [ ! "${ENV_FILE}" == "" ]
    then
        cmd="${cmd} --env-file ${ENV_FILE}"
    fi
    echo "${cmd}"
}
