###########################################################################
# Default adhoc function
###########################################################################
function adhoc() {
    echo "NOTICE: adhoc function has not been overridden"
}


###########################################################################
# Delete dangling docker images and volumes.
###########################################################################
function docker_prune() {
    docker image prune --force
    docker volume prune --force
}


###########################################################################
# Display Docker images
###########################################################################
function docker_images() {
    docker images --all "${IMAGE_NAME}"
}


###########################################################################
# Display Docker containers
###########################################################################
function docker_containers() {
    docker ps --all --filter "name=${CONTAINER_NAME}"
}


###########################################################################
# Enter the shell of a running Docker container
###########################################################################
function docker_shell() {
    docker exec -it "${CONTAINER_NAME}" "${CONTAINER_SHELL}"
}
