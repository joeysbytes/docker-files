# Docker Developer Assistant

## Usage

```bash
./run.bash dockerProject action
```

Parameters:
1) dockerProject: Directory under docker_files that contains the Docker project being tested
2) action:

| Action     | Description                                   | Variables                      | Var Description                                                                                                  |
|------------|-----------------------------------------------|--------------------------------|------------------------------------------------------------------------------------------------------------------|
| adhoc      | Run adhoc function (define in variables file) |                                |                                                                                                                  |
| build      | Build the Docker image                        | BUILD_NO_CACHE<br />BUILD_PULL | empty: use cache, non-empty: do not use cache<br />empty: do not pull newer images, non-empty: pull newer images |
| containers | List containers                               |                                |                                                                                                                  |
| down       | Stop Docker container(s)                      |                                |                                                                                                                  |
| images     | List images                                   |                                |                                                                                                                  |
| prune      | Remove dangling images and volumes            |                                |                                                                                                                  |
| shell      | Enter shell prompt of container               | CONTAINER_SHELL                | Command to execute to get shell prompt                                                                           |
| up         | Start the Docker container(s)                 | UP_DETACH                      | empty: start attached, non-empty: start detached                                                                 |
