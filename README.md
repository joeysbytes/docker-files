# Docker Files

My personal Dockerfile projects.

| Project     | Description                              |
|-------------|------------------------------------------|
| ansible     | Ansible image with convenience utilities |
| python-base | Base image for Python projects           |
